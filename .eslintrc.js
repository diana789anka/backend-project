module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint', 'import', 'prettier'],
	extends: [
		'airbnb-typescript/base',
		'plugin:@typescript-eslint/eslint-recommended',
		'prettier',
	],
	env: {
		browser: true,
		amd: true,
		node: true,
		es6: true,
	},
	parserOptions: {
		project: ['./tsconfig.eslint.json', './tsconfig.json'],
		ecmaVersion: 12,
	},
	rules: {
		'prettier/prettier': ['error'],
	},
};
