# Backend Project

Node js training project

## Prerequisites

1. [Node.js](https://nodejs.org/en/)
2. [npm](https://www.npmjs.com/)

## To Use

git clone <https://gitlab.com/diana789anka/backend-project.git>

cd Training_Project

npm install

npm start
